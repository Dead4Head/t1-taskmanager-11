package ru.t1.amsmirnov.taskmanager.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void showTaskById();

    void showTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void clearTasks();

    void removeTasksById();

    void removeTasksByIndex();

}