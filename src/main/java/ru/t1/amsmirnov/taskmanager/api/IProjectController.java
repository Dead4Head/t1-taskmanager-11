package ru.t1.amsmirnov.taskmanager.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void showProjectById();

    void showProjectByIndex();
    
    void updateProjectById();

    void updateProjectByIndex();

    void clearProjects();

    void removeProjectById();

    void removeProjectByIndex();

}