package ru.t1.amsmirnov.taskmanager.api;

import ru.t1.amsmirnov.taskmanager.model.Task;

public interface ITaskService extends ITaskRepository {

    Task createTask(String name, String description);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);
}
