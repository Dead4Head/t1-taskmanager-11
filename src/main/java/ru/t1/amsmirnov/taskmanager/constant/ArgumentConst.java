package ru.t1.amsmirnov.taskmanager.constant;

public final class ArgumentConst {

    public static final String HELP = "-h";
    public static final String ABOUT = "-a";
    public static final String INFO = "-i";
    public static final String VERSION = "-v";
    public static final String COMMANDS = "-cmd";
    public static final String ARGUMENTS = "-args";

    private ArgumentConst() {
    }

}
